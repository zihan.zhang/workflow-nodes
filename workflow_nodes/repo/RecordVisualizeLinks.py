# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import click
from graphviz import Digraph
from kadi_apy.cli.utils import raise_request_error
from kadi_apy.lib.records import Record
from xmlhelpy import Choice
from xmlhelpy import command
from xmlhelpy import Integer
from xmlhelpy import option
from xmlhelpy import String

from .utils import repo_command


@command(
    name="RecordVisualizeLinks",
    version="0.0.1",
)
@repo_command
@option(
    "start_id",
    char="i",
    required=True,
    description="The ID of the record to start with",
    param_type=Integer,
)
@option(
    "links_level",
    char="l",
    description="To visualize k level nearest neighbor links",
    default=2,
    param_type=Integer,
)
@option(
    "output_filename",
    char="n",
    required=True,
    default="record_links_graph",
    description="Filename of the record graph",
    param_type=String,
)
@option(
    "output_format",
    char="f",
    description="Output format of the record graph",
    default="svg",
    param_type=Choice(["svg", "pdf"]),
)
def execute(start_id, links_level, output_filename, output_format):
    """Visualize the links of a given record"""

    id_list = [start_id]
    id_list_current_level = [start_id]

    while links_level > 0:
        id_list_next_level = []

        for _id in id_list_current_level:

            items_to = Record(id=_id).get_record_links(direction="to").json()["items"]
            items_from = (
                Record(id=_id).get_record_links(direction="from").json()["items"]
            )

            for item in items_from:
                id_list_next_level.append(item["record_from"]["id"])
            for item in items_to:
                id_list_next_level.append(item["record_to"]["id"])

        id_list_current_level = list(set(id_list_next_level))
        id_list = id_list + id_list_next_level
        links_level = links_level - 1

    id_list = list(set(id_list))

    dot = Digraph(
        comment=f"Links starting from record {start_id}",
        format=output_format,
        node_attr={"color": "lightblue2", "style": "filled"},
    )

    for id in id_list:

        r = Record(id=id)
        meta = r.meta

        dot.node(
            f"{r.id}",
            f"id: {r.id} {meta['identifier']}",
            shape="ellipse",
            href=meta["_links"]["self"].replace("/api", ""),
        )
        response = r.get_record_links()

        if response.status_code == 200:
            payload = response.json()

            for results in payload["items"]:

                try:
                    if (
                        results["record_to"]["id"] in id_list
                        and results["record_from"]["id"] in id_list
                    ):
                        dot.edge(
                            f"{results['record_to']['id']}",
                            f"{results['record_from']['id']}",
                            label=f"id: {results['id'] }  {results['name']}",
                        )
                    else:
                        pass

                except Exception as e:
                    click.echo(e)

        else:
            raise_request_error(response=response)

    dot.render(output_filename)


if __name__ == "__main__":
    execute()
