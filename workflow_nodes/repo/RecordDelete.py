# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi_apy.cli.utils import item_delete
from kadi_apy.lib.records import Record
from xmlhelpy import command
from xmlhelpy import option

from .utils import id_identifier_options
from .utils import repo_command


@command(
    name="RecordDelete", version="0.0.1", description="Wrapper node to delete a record"
)
@repo_command
@id_identifier_options(item="record", helptext="to delete")
@option(
    "i-am-sure", description="Enable this option to delete the record", is_flag=True
)
def execute(record_id, record_identifier, i_am_sure):
    """function to delete a record"""

    r = Record(identifier=record_identifier, id=record_id)

    item_delete(r, i_am_sure)


if __name__ == "__main__":
    execute()
