# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi_apy.cli.utils import item_add_user
from kadi_apy.lib.records import Record
from xmlhelpy import Choice
from xmlhelpy import command
from xmlhelpy import Integer
from xmlhelpy import option

from .utils import id_identifier_options
from .utils import repo_command


@command(
    name="RecordAddUser",
    version="0.0.1",
    description="Add a user to a record in Kadi4Mat",
)
@repo_command
@id_identifier_options(item="record")
@option(
    "user-id",
    char="u",
    required=True,
    description="ID of the user to add",
    param_type=Integer,
)
@option(
    "permission-new",
    char="p",
    description="Permission of new user",
    default="member",
    param_type=Choice(["member", "editor", "admin"]),
)
def execute(record_id, record_identifier, user_id, permission_new):
    """function to add a user to a record"""

    r = Record(id=record_id, identifier=record_identifier)

    item_add_user(r, user_id, permission_new)


if __name__ == "__main__":
    execute()
