# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi_apy.cli.utils import record_add_files
from kadi_apy.lib.records import Record
from xmlhelpy import command
from xmlhelpy import option

from .utils import id_identifier_options
from .utils import repo_command


@command(
    name="RecordAddFiles",
    version="0.0.1",
    description="Wrapper node to upload file(s) into Kadi4Mat",
)
@repo_command
@id_identifier_options(item="record")
@option(
    "file-name", char="n", required=True, description="Path to a file or directory."
)
@option(
    "pattern",
    char="p",
    description=(
        "Pattern to select certains files, e.g. *.txt. Only relevant when uploading the"
        " content of a directory."
    ),
    default="*",
)
@option(
    "force",
    char="f",
    description=(
        "Enable if existing file(s) with identical name(s) should be deleted before "
        "uploading"
    ),
    default=False,
    is_flag=True,
    as_string=True,
)
def execute(record_id, record_identifier, **kwargs):
    """function to upload file(s)"""

    r = Record(id=record_id, identifier=record_identifier)

    record_add_files(r, **kwargs)


if __name__ == "__main__":
    execute()
