# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi_apy.cli.utils import cli_add_metadata
from kadi_apy.lib.records import Record
from xmlhelpy import command
from xmlhelpy import option

from .utils import id_identifier_options
from .utils import repo_command


@command(
    name="RecordAddMetadata",
    version="0.0.1",
    description="Add metadata to a record in Kadi4Mat",
)
@repo_command
@id_identifier_options(item="record")
@option(
    "metadata",
    char="m",
    description="Metadata string input",
    as_string=True,
    default=None,
)
@option(
    "file",
    char="p",
    description="Path to file containing metadata",
    default=None,
)
@option(
    "force",
    char="f",
    description="Force overwriting existing metadata with identical names",
    is_flag=True,
    default=False,
)
def execute(record_id, record_identifier, metadata, file, force):
    """function to add metadata"""

    r = Record(identifier=record_identifier, id=record_id)

    cli_add_metadata(r=r, metadata=metadata, file=file, force=force)


if __name__ == "__main__":
    execute()
