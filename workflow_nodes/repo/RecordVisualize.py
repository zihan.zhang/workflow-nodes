# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import click
from graphviz import Digraph
from kadi_apy.cli.utils import raise_request_error
from kadi_apy.lib.core import SearchResource
from kadi_apy.lib.records import Record
from xmlhelpy import Choice
from xmlhelpy import command
from xmlhelpy import option
from xmlhelpy import String

from .utils import repo_command


@command(
    name="RecordVisualize",
    version="0.0.1",
)
@repo_command
@option(
    "record_range",
    char="r",
    description="Choose the record range, only the linked records or all records",
    default="links",
    param_type=Choice(["links", "all"]),
)
@option(
    "output_filename",
    char="n",
    default="record_graph",
    description="Filename of the record graph",
    param_type=String,
)
@option(
    "output_format",
    char="f",
    description="Output format of the record graph",
    default="svg",
    param_type=Choice(["svg", "pdf"]),
)
def execute(record_range, output_filename, output_format):
    """Visualize a user's records and their links"""

    resource = SearchResource()
    responce = resource.search_items_user(item=Record, user=resource.pat_user_id)
    payload = responce.json()
    total_pages = payload["_pagination"]["total_pages"]

    record_ids = []

    for i in range(total_pages):
        response = resource.search_items_user(
            item=Record, user=resource.pat_user_id, page=i + 1
        )
        payload = response.json()
        for item in payload["items"]:
            record_id = item["id"]
            r_c = Record(id=record_id)
            if record_range == "links":

                # single record without linkage will not be displayed
                if (
                    r_c.get_record_links(direction="to").json()["items"] == []
                    and r_c.get_record_links(direction="from").json()["items"] == []
                ):
                    continue

                record_ids.append(item["id"])

            # all records
            else:
                record_ids.append(item["id"])

    dot = Digraph(
        comment="user's records",
        format=output_format,
        node_attr={"color": "lightblue2", "style": "filled"},
    )

    for id in record_ids:

        r = Record(id=id)
        meta = r.meta

        dot.node(
            f"{r.id}",
            f"id: {r.id} {meta['identifier']}",
            shape="ellipse",
            href=meta["_links"]["self"].replace("/api", ""),
        )
        response = r.get_record_links()

        if response.status_code == 200:
            payload = response.json()

            for results in payload["items"]:
                try:
                    dot.edge(
                        f"{results['record_to']['id']}",
                        f"{results['record_from']['id']}",
                        label=f"id: {results['id'] }  {results['name']}",
                    )

                except Exception as e:
                    click.echo(e)

        else:
            raise_request_error(response=response)

    dot.render(output_filename)


if __name__ == "__main__":
    execute()
