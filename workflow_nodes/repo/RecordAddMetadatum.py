# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi_apy.cli.utils import record_add_metadatum
from kadi_apy.cli.utils import validate_metadatum
from kadi_apy.lib.records import Record
from xmlhelpy import Choice
from xmlhelpy import command
from xmlhelpy import option
from xmlhelpy import String

from .utils import id_identifier_options
from .utils import repo_command


@command(
    name="RecordAddMetadatum",
    version="0.0.1",
    description="Add a metadatum to a record in Kadi4Mat",
)
@repo_command
@id_identifier_options(item="record")
@option(
    "metadatum",
    char="m",
    required=True,
    description="Name of metadatum to add",
    param_type=String,
)
@option("value", char="v", required=True, description="Value of metadatum to add")
@option(
    "type",
    char="t",
    description="Type of metadatum to add",
    param_type=Choice(["string", "integer", "float", "boolean"]),
    default="string",
)
@option(
    "unit",
    char="u",
    description="Unit of metadatum to add",
    param_type=String,
    default=None,
)
@option(
    "force",
    char="f",
    description="Force overwriting existing metadatum with identical name",
    default=False,
    is_flag=True,
    as_string=True,
)
def execute(record_id, record_identifier, metadatum, value, type, unit, force):
    """function to add a metadatum"""

    r = Record(identifier=record_identifier, id=record_id)

    metadatum_new = validate_metadatum(
        metadatum=metadatum, value=value, type=type, unit=unit
    )

    record_add_metadatum(r, metadatum_new=metadatum_new, force=force)


if __name__ == "__main__":
    execute()
