# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi_apy.cli.utils import item_create
from kadi_apy.lib.records import Record
from xmlhelpy import Choice
from xmlhelpy import command
from xmlhelpy import option

from .utils import repo_command


@command(
    name="RecordCreate", version="0.0.1", description="Wrapper node to create a record"
)
@repo_command
@option(
    "identifier",
    char="i",
    required=True,
    description="Identifier of the record",
    default=None,
)
@option(
    "title",
    char="t",
    required=False,
    description="Title of the record",
    default="my title",
)
@option(
    "visibility",
    char="v",
    required=False,
    description="Visibility of the record",
    default="private",
    param_type=Choice(["private", "public"]),
)
@option(
    "pipe",
    char="p",
    description="Use this flag if you want to pipe the returned record id.",
    is_flag=True,
)
def execute(**kwargs):
    """function to create a record"""

    item_create(class_type=Record, **kwargs)


if __name__ == "__main__":
    execute()
