# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi_apy.cli.utils import record_get_file
from kadi_apy.lib.records import Record
from xmlhelpy import command
from xmlhelpy import option

from .utils import id_identifier_options
from .utils import repo_command


@command(
    name="RecordGetFile",
    version="0.0.1",
    description="Wrapper node to download one file or all files from a record",
)
@repo_command
@id_identifier_options(item="record", helptext="to download files from")
@option("file-name", char="n", description="Name of file to download")
@option("file-id", char="i", description="Id of file to download")
@option(
    "filepath",
    char="p",
    description="Path (folder) to store the file",
    default=".",
)
@option(
    "force",
    char="f",
    description=("Force overwriting file in the given folder"),
    default=False,
    is_flag=True,
    as_string=True,
)
def execute(record_id, record_identifier, file_name, file_id, filepath, force):
    """function fo download one file or all files from a record"""

    r = Record(id=record_id, identifier=record_identifier)

    record_get_file(r, filepath, force, file_name, file_id)


if __name__ == "__main__":
    execute()
