#!/usr/bin/python3
# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json

from kadi_apy.cli.utils import cli_add_metadata
from kadi_apy.cli.utils import item_add_tag
from kadi_apy.cli.utils import item_create
from kadi_apy.cli.utils import item_edit
from kadi_apy.cli.utils import record_add_files
from kadi_apy.cli.utils import record_get_file
from kadi_apy.lib.core import KadiAPI
from kadi_apy.lib.records import Record
from xmlhelpy import command
from xmlhelpy import option

from ..repo.utils import id_identifier_options
from ..repo.utils import repo_command


@command(
    name="Kadi2kadi",
    version="0.0.1",
    description="Reads a record from one kadi instance and transferns the content to"
    "another instance",
)
@repo_command
@id_identifier_options(item="record", helptext="to copy to the second instance")
@option(
    "force",
    char="f",
    description="Force deleting and overwriting existing data",
    is_flag=True,
)
@option(
    "host-2",
    char="H",
    description="Host name of the second Kadi4Mat instance to use for the API.",
    required=True,
)
@option(
    "token-2",
    char="K",
    description="Second personal access token (PAT) to use for the API.",
    required=True,
)
@option(
    "skip-verify-2",
    char="S",
    is_flag=True,
    description="Skip verifying the SSL/TLS certificate of the second host.",
)
@option(
    "file-path",
    char="p",
    description="File path for downloading/uploading files.",
    required=True,
)
def execute(
    record_id, record_identifier, force, host_2, token_2, skip_verify_2, file_path
):
    """Reads the data from a record of the first instance and copies basic data to the
    second instance."""

    r = Record(id=record_id, identifier=record_identifier)

    meta = r.meta

    KadiAPI.token = token_2
    KadiAPI.host = host_2
    KadiAPI.verify = skip_verify_2

    r_2 = item_create(
        class_type=Record, identifier=meta["identifier"], title=meta["title"]
    )

    item_edit(r_2, description=meta["description"])
    item_edit(r_2, type=meta["type"])

    for tag in meta["tags"]:
        item_add_tag(r_2, tag)

    cli_add_metadata(r_2, metadata=json.dumps(meta["extras"]), file=None, force=force)

    record_get_file(r, file_path, force)

    record_add_files(r_2, file_name=file_path, force=force, pattern="*")


if __name__ == "__main__":
    execute()
