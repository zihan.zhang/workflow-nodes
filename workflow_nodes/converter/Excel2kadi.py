#!/usr/bin/python3
# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import sys

from kadi_apy.cli.utils import item_add_collection_link
from kadi_apy.cli.utils import item_add_group_role
from kadi_apy.cli.utils import item_add_tag
from kadi_apy.cli.utils import item_add_user
from kadi_apy.cli.utils import item_create
from kadi_apy.cli.utils import item_edit
from kadi_apy.cli.utils import record_add_files
from kadi_apy.cli.utils import record_add_metadata
from kadi_apy.cli.utils import record_add_record_link
from kadi_apy.cli.utils import validate_metadatum
from kadi_apy.lib.exceptions import KadiAPYException
from kadi_apy.lib.records import Record
from openpyxl import load_workbook
from xmlhelpy import argument
from xmlhelpy import Choice
from xmlhelpy import command
from xmlhelpy import option
from xmlhelpy import Path
from xmlhelpy import String

from ..repo.utils import repo_command


def _read_value(ws, letter, number):
    value = ws[f"{letter}{number}"].value
    if value is None:
        return value
    return str(value).strip()


def _parse_list(value):
    if value is None:
        return value
    value = value.split(";")
    value = map(str.strip, value)
    value = [obj for obj in value if obj]
    return value


def _check_int(value, output):
    try:
        value = int(value)
    except ValueError:
        print(f"No valid integer given in {output}.")
        sys.exit(1)

    return value


@command(
    name="Excel2kadi",
    version="0.0.1",
    description="Reads an Excel sheet and tranfers th input into kadi",
)
@repo_command
@argument(
    name="file",
    description="The Excel file to read from.",
    param_type=Path(path_type="file", exists=True),
    required=True,
)
@option(
    "force",
    char="f",
    description="Force deleting and overwriting existing information",
    is_flag=True,
)
@option(
    "keep-formulas",
    char="k",
    description="Flag to indicate wether to print the formula of a given cell, even if"
    " a computed value is available.",
    is_flag=True,
)
@option(
    "start-column", char="S", description="Start column", param_type=String, default="E"
)
@option(
    "end-column", char="E", description="End column", param_type=String, default=None
)
@option(
    "permission-new",
    char="p",
    description="Permission of new user",
    default="member",
    param_type=Choice(["member", "editor", "admin"]),
)
@option(
    "base-path",
    char="b",
    description="Prefix path to be added in front of the files or paths"
    " specified in the Excel sheet",
    param_type=String,
    default=None,
)
def execute(
    file, force, keep_formulas, start_column, end_column, base_path, permission_new
):
    """Imports an Excel sheet, reads metadata and transfers them into kadi."""

    wb = load_workbook(filename=file, read_only=True, data_only=not keep_formulas)
    ws = wb.active

    if not end_column:
        end_column = start_column

    for i in range(ord(start_column), ord(end_column) + 1):

        identifier = _read_value(ws, chr(i), 1)
        title = _read_value(ws, chr(i), 2)
        description = _read_value(ws, chr(i), 3)
        type = _read_value(ws, chr(i), 4)
        tags = _parse_list(_read_value(ws, chr(i), 5))
        add_collections = _parse_list(_read_value(ws, chr(i), 6))
        add_groups = _parse_list(_read_value(ws, chr(i), 7))
        add_user = _parse_list(_read_value(ws, chr(i), 8))
        links = _parse_list(_read_value(ws, chr(i), 10))
        title_links = _parse_list(_read_value(ws, chr(i), 11))
        files = _parse_list(_read_value(ws, chr(i), 13))
        metadatum = _read_value(ws, chr(i), 17)

        if base_path:
            if base_path[-1] != os.sep:
                base_path = base_path + os.sep

        try:
            r = item_create(class_type=Record, identifier=identifier, title=title)
        except KadiAPYException as e:
            print("Creating a new or loading an exsisting record was not successful.")
            print(e)
            sys.exit(1)

        if title:
            item_edit(r, title=title)

        if description:
            item_edit(r, description=description)

        if type:
            item_edit(r, type=type)

        if tags:
            for tag in tags:
                item_add_tag(r, tag)

        if add_collections:
            for collection in add_collections:
                _check_int(collection, f"{chr(i)}{6}")
                item_add_collection_link(r, collection_id=collection)

        if add_groups:
            for group in add_groups:
                _check_int(group, f"{chr(i)}{7}")
                item_add_group_role(r, group_id=group, permission_new=permission_new)

        if add_user:
            for user in add_user:
                _check_int(user, f"{chr(i)}{8}")
                item_add_user(r, user_id=user, permission_new=permission_new)

        if files:
            for obj in files:
                if base_path:
                    obj = base_path + obj
                record_add_files(r, file_name=obj, force=force, pattern="*")

        if links:
            if len(links) != len(title_links):
                print(
                    f"Found {len(links)} entries for links but {len(title_links)}"
                    " titles. Please use the same number of entries."
                )
                sys.exit(1)
            for link_iter, title_iter in zip(links, title_links):
                _check_int(link_iter, f"{chr(i)}{10}")
                record_add_record_link(r, record_to=link_iter, name=title_iter)

        if metadatum:
            metadata = []
            x = 17
            while True:
                metadatum_key = _read_value(ws, "A", x)
                if not metadatum_key:
                    break
                metadatum_value = _read_value(ws, chr(i), x)
                metadatum_type = _read_value(ws, "C", x)
                metadatum_unit = _read_value(ws, "D", x)

                metadata.append(
                    validate_metadatum(
                        metadatum=metadatum_key,
                        value=metadatum_value,
                        type=metadatum_type,
                        unit=metadatum_unit,
                    )
                )
                x = x + 1

            record_add_metadata(r, metadata_new=metadata, force=force)


if __name__ == "__main__":
    execute()
