# Workflow Nodes

This is a collection of nodes usable inside a *workflow* written in Python 3.
There are nodes to generate reports, to wrap system commands like `awk` and
some nodes for special use cases like integrating ImageJ macros. Additionally,
it is possible to connect to a Kadi4Mat instance to upload and download data
and metadata.

Each node is an executable command line tool providing the `--xmlhelp`
interface.

## Installation

There are two methods of installing:

- Installing for development

    pip3 install -e .[dev]

- Installing as a third party package

    pip3 install .

This does the same but should only be used if you do not plan to change the
source files on your system. Read more
[here](https://stackoverflow.com/a/19048754/3997725).

## Usage

After the installation you can call individual nodes on the command line, e.g.:

    StartReport --title "My Report" --author "John Doe"
    TextReport --section "My first section" --text "My text"
    EndReport

Or you can add the nodes to the file `$HOME/.pacestudio/tools.txt` to install
them in the Workflow Editor. Afterwards you can insert them using the "Add
Tool" context menu option in the editor.

## How to create a new node

1. Create a .py file for the node in the desired package, e.g `nodes/report/`.
2. Implement a function `execute(*args, **kwargs)` and add the desired options
   and arguments using the annotations from `lib.click_wrapper`.
3. Add an entry point in `setup.py` to make your node executable after the
   package was installed:

   ```
    entry_points={
        "console_scripts": [
            "StartReport = nodes.report.StartReport:execute",
            ...
            // your entry here
        ]
    }
    ```

3. Update the installation by calling `pip3 install -e .` in the project root.
   Your node should now be callable via the command line.
